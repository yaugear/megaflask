# -*- coding: UTF-8 -*-
from flask import render_template, flash, redirect, url_for, request, g, jsonify
from flask.ext.login import login_user, logout_user, current_user, login_required

from app import app, login_manager, db
from app.forms import LoginForm, RegistrationForm
from app.models import User

@login_manager.user_loader
def load_user(id):
    """
        Used to get user by id
    """
    return User.query.get(int(id))

@app.before_request
def before_request():
    """
        Used to get current user
    """
    g.user = current_user

@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    """
        Example of simple view
        This page will be available as main page ('/' or '/index')
        As result shows templates/index.html template
    """
    return render_template("index.html",
                           title="Main",
                           user=g.user)


@app.route('/ajax', methods=['GET', 'POST'])
def ajax():
    """
        Example of view with ajax
        This page will be available at '/ajax'
        As result shows templates/ajax.html template
    """
    return render_template("ajax.html",
                           title="AJAX example",
                           user=g.user)

@app.route('/login', methods=['GET', 'POST'])
def login():
    """
        User login view
    """
    if g.user is not None:
        if g.user.is_authenticated:
            return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        username = request.form['username']
        password = request.form['password']
        remember_me = False
        if 'remember_me' in request.form:
            remember_me = True
        registered_user = User.query.filter_by(username=username, password=password).first()
        if registered_user is None:
            flash('Login or password are incorrect!', 'error')
            return redirect(url_for('login'))
        login_user(registered_user, remember = remember_me)
        flash('Success!')
        return redirect('/index')
    return render_template('login.html',
                           title='Login',
                           form=form)

@app.route('/logout')
def logout():
    """
        User logout view
    """
    logout_user()
    return redirect(url_for('index'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    """
        User registration view
    """
    if g.user is not None:
        if g.user.is_authenticated:
            return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(request.form['username'], request.form['password'], request.form['email'])
        db.session.add(user)
        db.session.commit()
        flash('Success!')
        return redirect('/login')
    return render_template('register.html',
                           title='Registration',
                           form=form,
                           user=g.user)

@app.errorhandler(404)
def page_not_found(e):
    """
        'Page not found' exception
    """
    return render_template('404.html',
                           user=g.user), 404

@app.route('/sayhello', methods=['POST'])
def sayhello():
    """
        Example of AJAX. 
        Server side gets name from POST request and returns 'Hello, %name%'
        FYI: for AJAX need to be callen as javascript function from HTML templates
        In this example, client side in templates/index.html
    """
    name = request.form['name']
    text = 'Hello, %s!' % name
    return jsonify({'text': text})

import os
basedir = os.path.abspath(os.path.dirname(__file__))

#MySQL
# SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:****@localhost/dbname'

# SQLLITE
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

CSRF_ENABLED = True
SECRET_KEY = 'you-will-never-guess2'

# administrator list
ADMINS = ['you@example.com']
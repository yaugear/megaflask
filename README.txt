Mega Flask
==========

Mega cборка Flask подготовлена по туториалу 
    http://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world.
На русском c Хабра:
    https://habrahabr.ru/post/193242/


Включает:
---------

- Flask (for Python >=2.7, 3.x)
- Bootstrap CSS
- SQLite (mysite/app.db - файл БД)

+ готовая регистрация и авторизация
+ заготовки (пример) Ajax
+ готовые скрипты для распаковки(restore.py) и дамп(dump.py)
+ готовые скрипты для создания и миграции БД (mysite/db_create.py, mysite/db_migrate.py), а также для upgrade(mysite/db_upgrade.py) и downgrade(mysite/db_downgrade.py)

+ заготовки REST API
    http://flask-restful-cn.readthedocs.io/en/0.3.4/quickstart.html


Планы по включению:
-------------------

+ заготовка AUTH 2.0
    https://habrahabr.ru/company/mailru/blog/115163/
    https://github.com/lepture/example-oauth2-server
+ заготовка Unit Test
+ авторизация через соц.сети
+ админка(?)


Для работы с pythonanywhere.com
-------------------------------

1. Зарегистрироваться на pythonanywhere.com
2. Начать проект flask (c python 3.4)
3. Через консоль поставить новое окружение с python 3.4
    https://help.pythonanywhere.com/pages/Virtualenvs
4. Установить все необходимые питон-библиотеки
    http://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world
    
$ pip install flask flask-login flask-sqlalchemy sqlalchemy-migrate flask-wtf flask-restful
(optional) $ pip install flask-mysqldb pymysql
    
5. Развернуть систему (распаковать архив dump.zip)
$ python restore.py

(!6) Запустить с консоли с папки mysite/ скрипт db_create.py
$ cd mysite
$ python db_create.py
    
(!7) С питоновской консоли создать супер-пользователя

>>>from app import db,models
>>>u = models.User('admin','admin','admin@gmail.com')
>>>db.session.add(u)
>>>db.session.commit()
    
8. Перезапустить приложение (reload)
9. Для дампа сайта:
$ python dump.py


Вопросы?
--------

Писать сюда ramil.gata@gmail.com